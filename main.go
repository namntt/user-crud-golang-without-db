package main

import (
	"net/http"

	"go/apis"

	"github.com/gorilla/mux"
)

func main() {
	router := mux.NewRouter()

	router.HandleFunc("/api/user/find", apis.FindUserById).Methods("GET")
	router.HandleFunc("/api/user/getall", apis.GetAllUser).Methods("GET")
	router.HandleFunc("/api/user/create", apis.CreateUser).Methods("POST")
	router.HandleFunc("/api/user/update", apis.UpdateUser).Methods("PUT")
	router.HandleFunc("/api/user/delete", apis.DeleteUser).Methods("DELETE")

	err := http.ListenAndServe(":5000", router)
	if err != nil {
		panic(err)
	}
}
