package models

import (
	"errors"

	"go/entities"
)

var (
	listUSer = make([]*entities.User, 0)
)

func FindUserById(id string) (*entities.User, error) {
	for _, user := range listUSer {
		if user.Id == id {
			return user, nil
		}
	}
	return nil, errors.New("user does not exist")
}

func GetAllUser() []*entities.User {
	return listUSer
}

func CreateUser(user *entities.User) bool {
	if user.Id != "" && user.Name != "" && user.Password != "" {
		if userF, _ := FindUserById(user.Id); userF == nil {
			listUSer = append(listUSer, user)
			return true
		}
	}
	return false
}

func UpdateUser(eUser *entities.User) bool {
	for i, user := range listUSer {
		if user.Id == eUser.Id {
			listUSer[i] = eUser
			return true
		}
	}
	return false
}

func DeleteUser(id string) bool {
	for i, user := range listUSer {
		if user.Id == id {
			//copy (replace) element i by all elements from i+1
			copy(listUSer[i:], listUSer[i+1:])
			//set last element of listUser by empty User
			listUSer[len(listUSer)-1] = &entities.User{}
			//update listUser by its element from 0 to len-1
			listUSer = listUSer[:len(listUSer)-1]
			return true
		}
	}
	return false
}
